package scripts;

//import java.awt.Robot;
//import java.awt.Toolkit;
//import java.awt.Window;
//import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
//import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.server.handler.SendKeys;

import scripts.util.Constants;
import scripts.util.PasswordEncryption;
import scripts.util.StopWatch;

public class BasicTest {
	
	WebDriver driver;
	
	private static Logger logger = Logger.getLogger(BasicTest.class);
	
	private long sessionId;
	
	/**
	 * Validates the devops-web application for availability of elements
	 * @throws InterruptedException
	 */
	public void testDevopsWeb() throws InterruptedException {
		String message = "FAILED";		
		WebElement validElement = null;
		ArrayList<String> appURLs = new ArrayList<String>();
		String localUrlPrefix = "http://localhost:8090";
		String slave1UrlPrefix = "http://10.1.151.29:8090";
		String slave2UrlPrefix = "http://10.1.151.41:8090";
		String appName = "devops-web";
		//appURLs.add(localUrlPrefix + Constants.FORWARD_SLASH + appName);
		appURLs.add(slave1UrlPrefix + Constants.FORWARD_SLASH + appName);
		appURLs.add(slave2UrlPrefix + Constants.FORWARD_SLASH + appName);
		
		String appName = "devops-web";
		appURLs.add(appUrlPrefix + Constants.FORWARD_SLASH + appName);
		
		for(int i = 0; i < appURLs.size(); i++) {
			StopWatch timer = new StopWatch();
			driver.get(appURLs.get(i));
			try {
				validElement = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "/html/body/h1/font");
				if(validElement != null) {
					System.out.println(validElement.getText());	
					if(validElement.getText().equals("Hello GE Grid!! Welcome to DevOps !!")) {
						validElement = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "/html/body/h2/b/font");
						if(validElement.getText().equals("-- A demonstration by QuEST Global")) {
							/*driver.get(appURLs.get(i) + "/LastDeployed.html");
							validElement = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "/html/body/span");
							if(validElement.getText().indexOf("Last Deployed at ") >= 0)*/
								message = "SUCCESS";
						}
					}
				}	
				logger.info("==>>" + sessionId + "<<==" + "Finished testDevopsWeb(" + appURLs.get(i) 
						+ ") in " + timer.elapsedTime() + " seconds with status: " + message);
			} catch (NoSuchElementException nsee) {
				logger.info("==>>" + sessionId + "<<==" + "Finished testDevopsWeb(" + appURLs.get(i) 
						+ ") in " + timer.elapsedTime() + " seconds with status: " + message);
				String errorMsg = nsee.getMessage();			
				logger.info("==>>" + sessionId + "<<==" + "testDevopsWeb(): Element Not Found => " 
						+ errorMsg.substring(errorMsg.indexOf("Unable"),errorMsg.indexOf("Unable") + 100));
			}
		}
	}

	/**
	 * Validates the accessibility of Google Search and GitHub pages
	 * @throws InterruptedException
	 */
	public void testGoogleSearch() throws InterruptedException {
		StopWatch timer = new StopWatch();
		String message = "FAILED";		
		driver.get("https://google.co.in");
		WebElement validElement = null;
		try {
			validElement = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "//*[@id=\"lst-ib\"]");
			if(validElement != null) {
				validElement.sendKeys("What is DevOps?");
				validElement.sendKeys(Keys.RETURN);			
				Thread.sleep(Constants.DRIVER_WAIT_FOR_VIEWING); // wait to view the results
				validElement = verifyElementEnabled(Constants.ELEMENT_LINKTEXT_STRING, "DevOps - Wikipedia");				
				if (verifyLinkEnabled(validElement)) {
					validElement.click();					
					Thread.sleep(Constants.DRIVER_WAIT_FOR_VIEWING);
					message = "SUCCESS";
				}
			}	
			logger.info("==>>" + sessionId + "<<==" + "Finished testGoogleSearch in " 
					+ timer.elapsedTime() + " seconds with status: " + message);
		} catch (NoSuchElementException nsee) {
			logger.info("==>>" + sessionId + "<<==" + "Finished testGoogleSearch in " 
					+ timer.elapsedTime() + " seconds with status: " + message);
			String errorMsg = nsee.getMessage();			
			logger.info("==>>" + sessionId + "<<==" + "testGoogleSearch(): Element Not Found => " 
					+ errorMsg.substring(errorMsg.indexOf("Unable"),errorMsg.indexOf("Unable") + 100));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void uploadToGitHub() {
		StopWatch timer = new StopWatch();
		String message = "FAILED";
		WebElement validElement = null;
		testGitHubSignIn();
		driver.get("https://github.com/veersudhir83/SeleniumTrials");
		try {			
			driver.get("https://github.com/veersudhir83/SeleniumTrials/upload/master");
			validElement = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, 
					"//*[@id=\"js-repo-pjax-container\"]/div[2]/div[1]/div[2]/form[2]/div[2]/p/input");
			if (verifyLinkEnabled(validElement)) {
				validElement.click();
				Thread.sleep(Constants.DRIVER_WAIT_FOR_VIEWING);
				//Running vb script to choose the file
				Runtime.getRuntime().exec( "wscript D:/1017141/STS-WORKSPACE/SeleniumTrials/chooseFile.vbs" );
				
				Thread.sleep(Constants.DRIVER_WAIT_FOR_REFRESH); // wait for upload to finish
				validElement = verifyElementEnabled(Constants.ELEMENT_ID_STRING, "commit-summary-input");
				if(validElement != null && validElement.isDisplayed() && validElement.isEnabled()) {
					validElement.sendKeys("Automatic Upload by Selenium");
					/*
					Actions builder = new Actions(driver);
					Action actionSeries = builder
							.moveToElement(validElement)
							.click()
							.keyDown(Keys.CONTROL)
							.sendKeys("a")
							.build();
					actionSeries.perform();
					//Thread.sleep(Constants.DRIVER_WAIT_FOR_VIEWING);
					*/
					
					validElement = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, 
							"//*[@id=\"js-repo-pjax-container\"]/div[2]/div[1]/form/button");
					validElement.click();	
					Thread.sleep(Constants.DRIVER_WAIT_FOR_REFRESH); // wait for page to refresh
					message = "SUCCESS";
				}
				
			}
			logger.info("==>>" + sessionId + "<<==" + "Finished uploadToGitHub in " 
				+ timer.elapsedTime() + " seconds with status: " + message);
			testGitHubSignOut();
		} catch (NoSuchElementException nsee) {
			logger.info("==>>" + sessionId + "<<==" + "Finished uploadToGitHub in " 
				+ timer.elapsedTime() + " seconds with status: " + message);
			String errorMsg = nsee.getMessage();			
			logger.info("==>>" + sessionId + "<<==" + "uploadToGitHub(): Element Not Found => " 
				+ errorMsg.substring(errorMsg.indexOf("Unable"),errorMsg.indexOf("Unable") + 100));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Validates SignIn to GitHub using credentials and then Sign Out
	 */
	public void testGitHubSignIn() {
		StopWatch timer = new StopWatch();
		String message = "FAILED";
		WebElement validElement = null;
		driver.get("https://github.com/veersudhir83");
		try {			
			validElement = verifyElementEnabled(Constants.ELEMENT_CSSSELECTOR_STRING, 
					"body > div.position-relative.js-header-wrapper > header > div > div > div > a:nth-child(2)");
			if(verifyLinkEnabled(validElement) && validElement.getText().equals("Sign in")) {
				validElement.click();
				WebElement userName = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "//*[@id=\"login_field\"]");
				WebElement password = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "//*[@id=\"password\"]");
				WebElement signInButton = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "//*[@id=\"login\"]/form/div[4]/input[3]");
				if (userName != null && password != null && signInButton != null) {
					PasswordEncryption penc = new PasswordEncryption();
					userName.sendKeys(penc.decryptPassword("dmVlcnN1ZGhpcjgz"));
					password.sendKeys(penc.decryptPassword("U21pbGV5Qm95ODM="));
					signInButton.click();			
					message = "SUCCESS";
				}
			}		
			logger.info("==>>" + sessionId + "<<==" + "Finished testGitHubSignIn in " + timer.elapsedTime() + " seconds with status: " + message);
		} catch (NoSuchElementException nsee) {
			logger.info("==>>" + sessionId + "<<==" + "Finished testGitHubSignIn in " + timer.elapsedTime() + " seconds with status: " + message);
			String errorMsg = nsee.getMessage();			
			logger.info("==>>" + sessionId + "<<==" + "testGitHubSignIn(): Element Not Found => " + errorMsg.substring(errorMsg.indexOf("Unable"),errorMsg.indexOf("Unable") + 100));
		}
	}
	
	/**
	 * Validates SignIn to GitHub using credentials and then Sign Out
	 */
	public void testGitHubSignOut() {
		StopWatch timer = new StopWatch();
		String message = "FAILED";
		try {			
			WebElement userDropDownMenu = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "//*[@id=\"user-links\"]/li[3]/a/span");
			if(userDropDownMenu != null) {
				userDropDownMenu.click();
				WebElement signOutMenuItem = verifyElementEnabled(Constants.ELEMENT_XPATH_STRING, "//*[@id=\"user-links\"]/li[3]/div/div/form/button", false);
				if (signOutMenuItem != null) {
					signOutMenuItem.click();
					message = "SUCCESS";
				}
			}
			logger.info("==>>" + sessionId + "<<==" + "Finished testGitHubSignOut in " + timer.elapsedTime() + " seconds with status: " + message);
		} catch (NoSuchElementException nsee) {
			logger.info("==>>" + sessionId + "<<==" + "Finished testGitHubSignOut in " + timer.elapsedTime() + " seconds with status: " + message);
			String errorMsg = nsee.getMessage();			
			logger.info("==>>" + sessionId + "<<==" + "testGitHubSignOut(): Element Not Found => " + errorMsg.substring(errorMsg.indexOf("Unable"),errorMsg.indexOf("Unable") + 100));
		}
	}
	
	/**
	 * Reflection method used to invoke appropriate test case
	 * @param object
	 * @param method
	 * @throws Exception
	 */
	public void invokeMethod(Object object, Method method) throws Exception {    
		PropertyConfigurator.configure("log4j.properties");
        method.invoke(object);
    }
	
	/**
	 * Loads Chrome driver for input machine
	 * @param machine
	 * @throws IOException
	 */
	public void loadChromeDriver(String machine) throws IOException {
		
		ClassLoader classLoader = getClass().getClassLoader();
	    URL resource = null;
	    
	    if(machine != null && machine.equals(Constants.MACHINE_WINDOWS)) 
	    	resource = classLoader.getResource("resources/drivers/" + machine.toLowerCase() + "_64/chromedriver.exe");
	    else if(machine != null && machine.equals(Constants.MACHINE_LINUX)) 
	    	resource = classLoader.getResource("resources/drivers/" + machine.toLowerCase() + "_64/chromedriver");
	    
	    File chromeDriver = new File(resource.getFile());
	    chromeDriver.setExecutable(true);
	    System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
	    
	    ChromeOptions options = new ChromeOptions();
	    options.addArguments("--incognito");
	    options.addArguments("test-type");
	    options.addArguments("start-maximized");
	    options.addArguments("--enable-automation");
	    options.addArguments("test-type=browser");
	    options.addArguments("disable-infobars");
	    
	    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
	    /*if(enableProxy) 
	    	capabilities.setCapability(CapabilityType.PROXY, new Proxy().setHttpProxy(Constants.NETWORK_PROXY));*/
	    driver = new ChromeDriver(capabilities);
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	/**
	 * Loads Firefox driver for input machine
	 * @param machine
	 * @throws IOException
	 */
	public void loadFirefoxDriver(String machine) throws IOException {
		URL resource = null;
		ClassLoader classLoader = getClass().getClassLoader();
		
		if (machine != null && machine.equals(Constants.MACHINE_WINDOWS))
		    resource = classLoader.getResource("resources/drivers/" + machine.toLowerCase() + "_64/geckodriver.exe");
		else if(machine != null && machine.equals(Constants.MACHINE_LINUX))
			resource = classLoader.getResource("resources/drivers/" + machine.toLowerCase() + "_64/geckodriver");

		File fireFoxDriver = new File(resource.getFile());
	    fireFoxDriver.setExecutable(true);
	    System.setProperty("webdriver.gecko.driver", fireFoxDriver.getAbsolutePath());
	    
	    File file = new File(classLoader.getResource("resources/drivers/firebug-2.0.19.xpi").getFile());
		file.setExecutable(true);
		
		ProfilesIni profile = new ProfilesIni();
		FirefoxProfile firefoxProfile = profile.getProfile("default");
		if(firefoxProfile == null) // if default profile is not found - create a new profile
			firefoxProfile = new FirefoxProfile();
	    firefoxProfile.addExtension(file);
	    firefoxProfile.setPreference("extensions.firebug.currentVersion", "2.0.19"); // Avoid startup screen
	    firefoxProfile.setPreference("browser.privatebrowsing.autostart", true);
	    /*
	    if(enableProxy) {
	    	firefoxProfile.setPreference("network.proxy.type", 1);	    
	    	firefoxProfile.setPreference("network.proxy.http", Constants.NETWORK_PROXY_URL);
	    	firefoxProfile.setPreference("network.proxy.http_port", Constants.NETWORK_PROXY_PORT);
	    	firefoxProfile.setPreference("network.proxy.https", Constants.NETWORK_PROXY_URL);
	    	firefoxProfile.setPreference("network.proxy.https_port", Constants.NETWORK_PROXY_PORT);
	    } 
		*/
	    driver = new FirefoxDriver(firefoxProfile);
	    
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	/**
	 * Closes the driver and kills any hanging processes after completion of test cases
	 * @param browser
	 */
	public void closeDriver(String browser) {
		driver.get("about:config");
		driver.quit();
		try {			
			Runtime rt = Runtime.getRuntime();
		    if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1) {
		    	System.out.println("Killing driver process for windows");
		    	if(browser != null && browser.equals(Constants.BROWSER_CHROME)) {
		    		rt.exec("taskkill " + "chromedriver.exe");
		    		System.out.println("Killed chromedriver.exe");
		    	} else if(browser != null && browser.equals(Constants.BROWSER_FIREFOX)) {
		    		rt.exec("taskkill " + "geckodriver.exe");
		    		System.out.println("Killed geckodriver.exe");
		    	}
		    } else {
		    	System.out.println("Killing driver process for linux");
		    	if(browser != null && browser.equals(Constants.BROWSER_CHROME)) {
		    		rt.exec("kill -9 " + "chromedriver");
		    		System.out.println("Killed chromedriver");
		    	} else if(browser != null && browser.equals(Constants.BROWSER_FIREFOX)) {
		    		rt.exec("kill -9 " + "geckodriver");
		    		System.out.println("Killed geckodriver");
		    	}
		    		
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
		// Delete all driver files		
		try {
			FileUtils.deleteDirectory(new File("Driver"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private WebElement verifyElementEnabled(String verifyBy, String verifyString) {
		// Default one that will not scroll to the element
		WebElement element = verifyElementEnabled(verifyBy, verifyString, true);
		return element;
	}
	
	/**
	 * Verifies if the WebElement is available and active at the input xpath and highlights it
	 * @param xpath
	 * @return
	 */
	private WebElement verifyElementEnabled(String verifyBy, String verifyString, boolean scrollToElement) {				
		WebElement element = null;
		switch (verifyBy) {
			case "xpath": 
				element = driver.findElement(By.xpath(verifyString));
				break;
			case "id":
				element = driver.findElement(By.id(verifyString));
				break;
			case "cssSelector":
				element = driver.findElement(By.cssSelector(verifyString));
				break;
			case "linkText":
				element = driver.findElement(By.linkText(verifyString));
				break;
			default:
				break;
		}
		if (element != null) {
			if (element.isDisplayed() && element.isEnabled()) {
				try {
			        JavascriptExecutor jse = (JavascriptExecutor) driver;
			        if(scrollToElement) 
			        	jse.executeScript("arguments[0].scrollIntoView(true);", element);
			        jse.executeScript("arguments[0].style.border='2px solid red'", element);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return element;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Verifies if the WebElement is available and active - used only to validate links
	 * @param element
	 * @return
	 */
	private boolean verifyLinkEnabled(WebElement element) {
		boolean available = false;
		if (element.isDisplayed() && element.isEnabled()) {
			available = true;
		}
		return available;
	}

	public long getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	
}
