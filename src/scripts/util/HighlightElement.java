package scripts.util;

//import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HighlightElement {
	public void flash(WebElement element, WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		String bgcolor  = element.getCssValue("backgroundColor");
		for (int i = 0; i < 2; i++) {
			changeColor("rgb(300,0,0)", element, js);
			changeColor(bgcolor, element, js);
		}
	}
	public void changeColor(String color, WebElement element,  JavascriptExecutor js) {
		js.executeScript("arguments[0].style.backgroundColor = '"+color+"'",  element);

		try {
			Thread.sleep(60);
		}  catch (InterruptedException e) {
		}
	}
	
	/*public void drawBorder(WebDriver driver, String xpath){
		try {
	        WebElement element_node = driver.findElement(By.xpath(xpath));
	        JavascriptExecutor jse = (JavascriptExecutor) driver;
	        jse.executeScript("arguments[0].style.border='3px solid red'", element_node);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }*/
}
