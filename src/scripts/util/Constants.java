package scripts.util;

public class Constants {
	public static String EMPTY_STRING = "";
	public static String MACHINE_LINUX = "LINUX";
	public static String MACHINE_WINDOWS = "WINDOWS";
	public static String BROWSER_FIREFOX = "FIREFOX";
	public static String BROWSER_CHROME = "CHROME";
	
	public static String NETWORK_PROXY = "proxy-tvm:8080";
	public static String NETWORK_PROXY_URL = "proxy-tvm";
	public static String NETWORK_PROXY_PORT = "8080";
	
	public static String ELEMENT_XPATH_STRING = "xpath";
	public static String ELEMENT_ID_STRING = "id";
	public static String ELEMENT_CSSSELECTOR_STRING = "cssSelector";
	public static String ELEMENT_LINKTEXT_STRING = "linkText";
	
	public static long DRIVER_WAIT_FOR_REFRESH = 10000;
	public static long DRIVER_WAIT_FOR_VIEWING = 3000;
	
	public static String FORWARD_SLASH = "/";
}