package scripts;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import scripts.util.Constants;

public class Starter {
	
	private static Logger logger = Logger.getLogger(Starter.class);
	
	private static String machine = Constants.MACHINE_LINUX; //Default WINDOWS
	private static String browser = Constants.BROWSER_CHROME; //Default FIREFOX
	
	private static long sessionId;
	
	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		sessionId = System.currentTimeMillis();
		logger.info("==>>" + sessionId + "<<==" + "Started Selenium Automated Testing");
		Starter start = new Starter();
		ArrayList<String> methodNames = new ArrayList<String>();
		
		//methodNames.add("testGoogleSearch");
		//methodNames.add("testGitHubSignIn");
		//methodNames.add("testGitHubSignOut");
		methodNames.add("testDevopsWeb");
		//methodNames.add("uploadToGitHub");
		if(args.length == 2) {
			machine = args[0].toUpperCase(); //Constants.MACHINE_WINDOWS;
			browser = args[1].toUpperCase(); //Constants.BROWSER_CHROME;
		}
		logger.info("System=" + machine);
		logger.info("Browser=" + browser);
		start.runTests(methodNames);
		logger.info("==>>" + sessionId + "<<==" + "Finished Selenium Automated Testing" + "\n======================================");
	}
	
	public void runTests(ArrayList<String> methodNames) {		
		
		try {
			BasicTest test = new BasicTest();
			test.setSessionId(sessionId);
			Method method;
			try {
				if(browser != null && browser.equals(Constants.BROWSER_CHROME)) {
					test.loadChromeDriver(machine);
				} else if(browser != null && browser.equals(Constants.BROWSER_FIREFOX)) {
					test.loadFirefoxDriver(machine);
				}
				for(int i=0; i < methodNames.size(); i++) {
					method = BasicTest.class.getMethod(methodNames.get(i).toString());
					test.invokeMethod(test, method);	
				}
				test.closeDriver(browser);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
